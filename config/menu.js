exports.items = [
  {
    label: 'Cuentas',
    icon: 'description',
    value: '/bills'
  },
  {
    label: 'Productos',
    icon: 'grade',
    value: '/products'
  },
  {
    label: 'Proveedores',
    icon: 'local_shipping',
    value: '/providers'
  },
  {
    label: 'Stock',
    icon: 'show_chart',
    value: '/stock'
  },
  {
    label: 'Sucursales',
    icon: 'store',
    value: '/stores'
  }
];