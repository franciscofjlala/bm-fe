const React = require("react");
const ReactDOM = require("react-dom");
const { Router, Route, IndexRoute, hashHistory } = require("react-router");
const injectTapEventPlugin = require('react-tap-event-plugin');
const getMuiTheme = require('material-ui/styles/getMuiTheme').default;
const MuiThemeProvider = require('material-ui/styles/MuiThemeProvider').default;

injectTapEventPlugin();

// const Favorites = require("./pages/Favorites");
// const Todos = require("./pages/Todos");
// const Settings = require("./pages/Settings");
const { Layout } = require("./components/Layout/index.js");

const app = document.getElementById('bmapp');

ReactDOM.render(
  <MuiThemeProvider muiTheme={getMuiTheme()}>
    <Router history={hashHistory}>
      <Route path="/" component={Layout}>
        <Route path="favorites" component={(props) => { return <div>'Hola'</div>}}></Route>
      </Route>
    </Router>
  </MuiThemeProvider>,
app);