exports.userContainer = (palette) => {
  return {
    height: '100px',
    padding: '15px',
    backgroundColor: palette.primary1Color,
    display: 'table',
    width: '100%',
    verticalAlign: 'middle',
    boxSizing: 'border-box'
  };
};

exports.userWrapper = () => {
  return {
    display: 'table-cell',
    width: '100%',
    verticalAlign: 'middle',
  };
};

exports.userPhoto = () => {
  return {
    display: 'table-cell',
    verticalAlign: 'middle',
    paddingRight: '20px'
  };
};

exports.userParagraph = (palette) => {
  return {
    margin: 0,
    backgroundColor: palette.primary1Color
  };
};

exports.userLink = (palette) => {
  return {
    color: palette.secondaryTextColor,
    fontSize: '12px',
    textDecoration: 'none'
  };
};

exports.menuIcon = (palette) => {
  return {
    verticalAlign: 'middle', 
    fontSize:'20px', 
    color: palette.secondaryTextColor
  };
};

exports.menuLabel = (palette) => {
  return {
    verticalAlign: 'middle',
    position: 'relative',
    top: '2px',
    paddingLeft: '20px',
  };
};
