const React = require('react');
const AppBar = require('material-ui/AppBar').default;
const Drawer = require('material-ui/Drawer').default;
const MenuItem = require('material-ui/MenuItem').default;
const List = require('material-ui/List').default;
const ListItem = require('material-ui/List').ListItem;
const FontIcon = require('material-ui/FontIcon').default;
const Subheader = require('material-ui/Subheader').default;
const Divider = require('material-ui/Divider').default;
const getMuiTheme = require('material-ui/styles/getMuiTheme').default;
const palette = getMuiTheme().palette;

const {
  items,
} = require('../../../config/menu.js');

console.log(items);

const {
  userContainer,
  userParagraph,
  userLink,
  userWrapper,
  userPhoto,
  menuIcon,
  menuLabel,
} = require('../../styles/layout');

export class Layout extends React.Component {
  constructor(props) {
    super();
    this.drawerHandler = this.drawerHandler.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    
    this.state = {
      drawerOpen: false,
      items: items,
    };
  }

  drawerHandler() {
    this.setState({
      drawerOpen: !this.state.drawerOpen,
    });
  }

  closeDrawer() {
    this.setState({
      drawerOpen: false,
    });
  }
  
  render() {
    return (
      <div>
        <AppBar 
          title="BM"
          onLeftIconButtonTouchTap={this.drawerHandler}/>
        <Drawer open={this.state.drawerOpen}
          docked={false}
          onRequestChange={this.closeDrawer} >
          <div className="user__container" style={userContainer(palette)}>
            <div className="user__photo" style={userPhoto(palette)}>
              <FontIcon className="material-icons">account_circle</FontIcon>
            </div>
            <div className="user__wrapper" style={userWrapper(palette)}>
              <p style={userParagraph(palette)}>Francisco Lala</p>
              <a href="#" style={userLink(palette)}>Cerrar Sesión</a>
            </div>
          </div>
          <List>
            {this.state.items.map(item =>   
              <ListItem key={Math.random()} value={item.value} href={`#${item.value}`}>
                <FontIcon className="material-icons menu__icon" style={menuIcon(palette)}>{item.icon}</FontIcon> 
                <span className="menu__label" style={menuLabel()}>{item.label}</span>
              </ListItem>
            )}
            <Divider />
            <Subheader>
              Personalizá
            </Subheader>
            <ListItem>
              <FontIcon className="material-icons menu__icon" style={menuIcon(palette)}>settings</FontIcon> 
              <span className="menu__label" style={menuLabel()}>Configuración</span>
            </ListItem>
          </List>
        </Drawer>
      </div>
    )
  }
}
